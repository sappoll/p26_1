 # Смена статуса в заявке ТМК при отменен записи в СЗПВ
https://jira.netrika.ru/browse/TELEMED-1828[https://jira.netrika.ru/browse/TELEMED-1828]

Цель. Требуется придумать механизм реакции tmcore на событие в СЗПВ (отмену записи). Отмена записи в В СЗПВ должна отображаться в логике заявки tmcore, смена статуса заявки автоматически.

Возможно тут решение может быть простым и отмена записи в СЗПВ на ТМК должна осуществляться только через tmcore, который в свою очередь будет выполнять работы с СЗПВ (tmcore и tmplugins уже умеют).

Возможно нужно делать микросервис, в который СЗПВ будет обращаться для отмены заявки по её ID. Микросервис будет при таком обращении должен выяснить с к какой предметной области принадлежит заявка, с какой ролью он может обратиться для выяснения возможности совершить изменение над заявкой, выяснить переход, доступный есму для изменения, выполнить данный переход. Плюс данный переход должен быть запроектирован в определенной конвенции на маршруте tmcore. 

Возможны другие варианты...

 # Автоматическая смена статуса при регистрации просрочки
https://jira.netrika.ru/browse/TELEMED-1979[https://jira.netrika.ru/browse/TELEMED-1979]

Нужно придумать придумать как автоматически менять статус заявки, если она просрочена. 

Например, есть запрос об автоматическом закрытии заявки со слишком давним сроком давности. 

Или есть запрос на то, чтобы не учитывать слишком старые заявки в логике проверки на дубли, которая в свою очередь умеет опираться на конечные статусы заявок.

Можем ли мы такую функциональность реализовать не наделяя просрочки исключительными правами в TMCore или нам наоборот следует сделать просрочкам такую функциональность на ровне с transition.  

 # PlantUML test

```plantuml
skinparam shadowing false
'skinparam monochrome true
skinparam borderColor #f3f3f3
skinparam {
	BoxPadding 40
        ParticipantPadding 10
	DefaultFontSize 16
	NoteTextAlignment center

}
skinparam sequence {
    ArrowFontColor #53802e
    BoxFontColor #53802e
    ArrowColor #444444
    LifeLineBorderColor #5b9bd5
    GroupBorderColor #53802e
    GroupBodyBackgroundColor Transparent
    GroupHeaderFontColor #53802e
    DividerBorderColor #53802e
    DividerBackgroundColor Transparent
    DividerFontColor #53802e
}

skinparam participant {
    backgroundColor #5b9bd5
    borderColor #5b9bd5
    fontColor #FFFFFF
}
skinparam sequenceMessageAlign center
skinparam sequenceBoxBorderColor #f3f3f3
!define mis1 "<b>МИС МО 1</b>\n(направляющая)"
!define mis2 "<b>МИС МО 2</b>\n(целевая)"
!define HcS "<b>Модуль профилей</b>\n<b>обслуживания</b>"
!define UP "Управление\nпотоками"
!define NoPasaran Заявка не соответствует профилю

participant mis1
box "ТМ" #f3f3f3
participant HcS
participant UP
end box
participant mis2

mis2 -> HcS: Запрос конфигурации\n **GET systems/config**
mis2 <<-- HcS: Конфигурация перечисляющая\nдоступные для составления условий справочники
mis2 -> HcS: Запрос **POST healthcareservice**\nна создание (изменение)\nпрофиля обслуживания
mis2 <<-- HcS: Данные\nсозданного профиля
mis1 -> HcS: Поиск профилей по параметрам\n**POST healthcareservice\_search**
mis1 <<-- HcS: Поисковая выдача\nподходящих профилей
mis1 -> HcS: Запрос конфигурации\n **GET systems/config**
mis1 <<-- HcS: Конфигурация соотносящая\nусловия профиля\nи атрибуты контекста заявки
...
== Работа с заявкой ==
mis1 -> UP: Запрос **POST StartNewProcess**\nна создание заявки в целевую МО\nпо выбранному профилю
UP -> HcS: Запрос к валидатору\nна проверку соответствия заявки\nвыбранному профилю
group NoPasaran
    UP <<-- HcS: Заявка не валидна\n"success": **false**
    UP -->x mis1: Сообщение об ошибке
end
UP <<-- HcS: Заявка валидна\n"success": **true**

mis1 <<-- UP: Созданная заявка
```
